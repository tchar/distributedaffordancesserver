package gr.ntua.softlab.affordances.endpoint;

import java.util.HashMap;
import java.util.Map;

import org.springframework.stereotype.Service;


@Service
public class ServerService {

	private static ServerService instance;
	
	private Map<String, Order> orders = new HashMap<>();
	
	private int ordersNum = 3;
	
	private ServerService(){
		sampleOrders();
	}
	
	public static ServerService getInstance(){
		if (instance == null){
			instance = new ServerService();
		}
		return instance;
	}

	public Order getOrder(String id){
		return orders.get(id);
	}

	
	private void sampleOrders(){
		orders.put("1", new Order("1", "A", 100));
		orders.put("2", new Order("2", "B", 200));
		orders.put("3", new Order("3", "C", 300));
	}

	public void deleteOrder(String id) {
		Order order = orders.remove(id);
		if (order != null){
			addOrder(order);
		}
	}

	public Order addOrder(Order order){
		orders.put(Integer.toString(++ordersNum), order);
		return orders.get(Integer.toString(ordersNum));
	}
	
}
