package gr.ntua.softlab.affordances.endpoint;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(value = HttpStatus.NOT_FOUND)
public class ResourceNotFoundException extends RuntimeException {

	public ResourceNotFoundException(){
		
	}
	
	public ResourceNotFoundException(String description) {
		super(description);
	}
	
	private static final long serialVersionUID = 599022991042261309L;
}