package gr.ntua.softlab.affordances.endpoint;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
@EnableAutoConfiguration
public class DistributedAffordancesServerApplication {

	public static void main(String[] args) {
		SpringApplication.run(DistributedAffordancesServerApplication.class, args);
	}
}
