package gr.ntua.softlab.affordances.endpoint;

import static de.escalon.hypermedia.spring.AffordanceBuilder.linkTo;
import static de.escalon.hypermedia.spring.AffordanceBuilder.methodOn;

//import static org.springframework.hateoas.mvc.ControllerLinkBuilder.linkTo;
//import static org.springframework.hateoas.mvc.ControllerLinkBuilder.methodOn;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.hateoas.Resource;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;


@Controller
@RequestMapping("/other-api/")
public class ServerController {

	@Autowired
	private ServerService service;
	
	
	@RequestMapping(value = "/order/{id}", method = RequestMethod.GET,
					produces = "application/ld+json", consumes = "application/ld+json")
	@ResponseBody
	public ResponseEntity<Resource<Order>> getOrder1(@PathVariable String id){
		return getOrder(id);
	}
	
	@RequestMapping(value = "/order/{id}", method = RequestMethod.GET,
					produces = "application/hal+json", consumes = "application/hal+json")
	@ResponseBody
	public ResponseEntity<Resource<Order>> getOrder2(@PathVariable String id){
		return getOrder(id);
	}
	
	@RequestMapping(value = "/order/{id}", method = RequestMethod.GET,
					produces = "application/json", consumes = "application/json")
	@ResponseBody
	public ResponseEntity<Resource<Order>> getOrder3(@PathVariable String id){
		return getOrder(id);
	}
	
	private ResponseEntity<Resource<Order>> getOrder(String id){
		
		Order order = service.getOrder(id);
		
		if (order == null){
			throw new ResourceNotFoundException("Order not found");
		}
		
		Resource<Order> resource = new Resource<>(order);
		
		resource.add(linkTo(methodOn(ServerController.class).getOrder1(id))
				.and(linkTo(methodOn(ServerController.class).deleteOrder(id))).withSelfRel());
		
		return ResponseEntity.ok().body(resource);
	}
	

	
	@RequestMapping(value = "/order/{id}", method = RequestMethod.DELETE,
					produces = "application/ld+json")
	@ResponseBody
	public ResponseEntity<Resource<Void>> deleteOrder(@PathVariable String id){
		service.deleteOrder(id);
		return ResponseEntity.ok(null);
	}

	@RequestMapping(value="/order", method = RequestMethod.POST,
					consumes = "application/json", produces = {"application/json"})
					ResponseEntity<Resource<Order>> postOrder(@RequestBody Order order){

		order = service.addOrder(order);
		Resource<Order> resource = new Resource<>(order);

		String id = order.getId();
		resource.add(linkTo(methodOn(ServerController.class).getOrder1(id))
				.and(linkTo(methodOn(ServerController.class).deleteOrder(id))).withSelfRel());

		return ResponseEntity.ok(resource);
	}
}
