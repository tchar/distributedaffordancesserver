package gr.ntua.softlab.affordances.endpoint;

public class Order {

	private String id;
	private String name;
	private int cost;
	
	public Order(){
		
	}
	
	public Order(String id, String name, int cost) {
		this.setId(id);
		this.setName(name);
		this.setCost(cost);
	}

	
	public String getId(){
		return id;
	}
	
	public void setId(String id){
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public int getCost() {
		return cost;
	}

	public void setCost(int cost) {
		this.cost = cost;
	}
	
}